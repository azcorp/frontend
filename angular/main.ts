/**
 * Created by zelenin on 26.08.16.
 */
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { enableProdMode } from '@angular/core';
import { MainModule } from './main.module';
import {WsMessage} from "./model/WsMessage";


if (process.env.ENV === 'production') {
    enableProdMode();
}
let ws = new WebSocket(`ws://${sessionStorage.getItem("server")}:8080/ws?access_token=${localStorage.getItem("token")}`);
let who=new WsMessage("who");
who.method="who";
who.params=null;

let settings=new WsMessage("setting");
settings.method="settings";
settings.params=null;
let block={who:true,settings:true};

ws.onclose=()=>{
    platformBrowserDynamic().bootstrapModule(MainModule);
    console.log(sessionStorage.getItem("profile"));
    console.log(sessionStorage.getItem("settings"));
};
ws.onmessage=(message)=>{
    let data = JSON.parse(message.data);
    if(data.id==who.id){
        sessionStorage.setItem("profile",JSON.stringify(data.result));
        block.who=false;
    }
    if(data.id==settings.id){
        sessionStorage.setItem("settings",JSON.stringify(data.result));
        block.settings=false;
    }

};
ws.onerror=(error)=>{
    console.log(error);
    localStorage.removeItem("token");
    location.href="/login.html"
};

ws.onopen=()=>{
    ws.send(JSON.stringify(who));
    ws.send(JSON.stringify(settings));
    new Promise((resolve)=>{
        let counter = setInterval(()=>{
            console.log("init wait");
            if(!block.who&&!block.settings){
                resolve(true);
                clearInterval(counter);
            }
        },2);
    }).then(()=>{
        ws.close()
    });
};








