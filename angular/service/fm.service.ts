/**
 * Created by zelenin on 07.09.16.
 */
import {Injectable, Inject} from '@angular/core';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';
import {Observable} from 'rxjs/Rx';
import {OutputService} from "./output.service";
import {Path} from "../model/Path"
import {RootService} from "./root.service";
import {ObserverService} from "./observer.service";
import * as _ from 'lodash';
import {WebSocketService} from "./websocket.service";
import {WsMessage, Query, Task, Order} from "../model/WsMessage";
import {VideoService} from "./video.service";


@Injectable()
export class FileManagerService{
    constructor(@Inject(OutputService)private  stdout:OutputService,
                @Inject(RootService)private rs:RootService,
                @Inject(ObserverService)private observers:ObserverService,
                @Inject(WebSocketService)private wss:WebSocketService,
                @Inject(VideoService)private  vs:VideoService){
                this.wss.ObservableWs.subscribe((data)=>{this.receive(data)});
    }

    private url = `${sessionStorage.getItem("protocol")}://${sessionStorage.getItem("server")}:8080/fs`;
    view:string="table";
    orderStack:Task[]=[];
    requestBlock:Task;
    buffer={block:false,list:[]};
    selected:Path[]=[];
    currentList:Path[]=[];
    editor={visible:false,title:"Text Editor",value:"",action: null};
    imgModal={visible:false,title:"Vieawer",list:[],img:null};
    video:boolean=false;
    fullPath:Path[]=[new Path(this.rs.profile.link,this.rs.profile.username)];
    receive(data){
        this.orderStack.forEach((task)=>{
            task.act(data)
        });
        if(this.requestBlock!=null)
        this.requestBlock.act(data);
    }
    removeTask(id){
        _.remove(this.orderStack,(o)=>{return o.id==id})
    }
    download(dbpath){
        window.location.href=`${sessionStorage.getItem("protocol")}://${sessionStorage.getItem("server")}:8880/${dbpath.inode}?filename=${dbpath.name.replace(/ /g,"_")}&size=${dbpath.size}`;
    }
    getLink(dbpath){return `${sessionStorage.getItem("protocol")}://${sessionStorage.getItem("server")}:8880/${dbpath.inode}?filename=${dbpath.name.replace(/ /g,"_")}&size=${dbpath.size}`}
    upload (files: File[],observerName:string,target): Observable<any> {
        return Observable.create(observer => {
            let formData: FormData = new FormData();
            formData.append("target",target);
            let xhr: XMLHttpRequest = new XMLHttpRequest();
            for (let i = 0; i < files.length; i++) {
                formData.append("files["+i+"]", files[i], files[i].name);
            }
            console.log(formData);
            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        observer.next(JSON.parse(xhr.response));
                        observer.complete();
                        this.update();
                    } else {
                        observer.error(xhr.response);
                    }
                }
            };

            xhr.upload.onprogress = (event)=>{
              this.observers.setValue(observerName,Math.round(event.loaded / event.total * 100));
            };
            xhr.upload.onloadend = ()=>{
                this.observers.setValue(observerName,0);
            };
            xhr.open('POST', `${this.url}/upload`, true);
            xhr.setRequestHeader('Authorization','Bearer '+localStorage.getItem("token"));
            xhr.send(formData);
        });

    }
    sendFiles(event){
        this.upload(event.target.files,"upload-progress",this.fullPath[this.fullPath.length-1].inode).subscribe(()=>{this.update();});
    }
    isSelectedContain(dbpath:Path){
        return _.findIndex(this.selected,dbpath) != -1;
    }
    isBlocked(path:Path){
        return ((_.findIndex(this.buffer.list, o=>{return o.inode==path.inode}) != -1))&&this.buffer.block;
    }
    setBuffer(list:Path[],block){
        this.buffer.list=list;
        this.buffer.block=block;
    }
    clearBuffer(){
        this.buffer.block=false;
        this.buffer.list=[];
    }
    selectItem($event,item){
        if($event.ctrlKey){
            if(this.selected.indexOf(item)!=-1){
                _.remove(this.selected,item)
            }else {
                this.selected.push(item)
            }
        }else if($event.shiftKey){
            if(this.selected.length>0){
                let a = this.currentList.indexOf(_.last(this.selected));
                let b = this.currentList.indexOf(item);
                this.selected=[];
                let delta = [];
                if(a<b)delta =  this.currentList.slice(a,b+1);else delta=this.currentList.slice(b,a+1);
                console.log(delta);
                delta.forEach(t=>{this.selected.push(t)})
            }else {
                this.selected.push(item)
            }
        }else
        {
            this.selected=[];
            this.selected.push(item);
        }
    };
    update(){
        let request=new WsMessage("fs"+this.wss.requestCounter);
        let query=new Query('path');
        query.equals=[{cr:'parent',val:`${this.fullPath[this.fullPath.length-1].inode}`}];
        query.sort=[{direct:"DESC",val:"isdir"},{direct:"ASC",val:"name"}];
        request.method="find";
        request.params=query;
        this.wss.send(request);
        this.requestBlock=new Task(request.id,(data)=>{
            this.currentList=data.result
        })
    }
    action(order) {
        if(this.orderStack.length<5){
            let request=new WsMessage("order"+this.wss.requestCounter);
            request.method="fsctrl";
            request.params=order;
            this.wss.send(request);
            this.orderStack.push(new Task(request.id,(data)=>{
                this.removeTask(data.id);
                this.update();
                this.selected=[];

            }))
        }
    }
    read(order){
        let request=new WsMessage("readOrder"+this.wss.requestCounter);
        request.method="fsctrl";
        request.params=order;
        this.wss.send(request);
        this.orderStack.push(new Task(request.id,(data)=>{
            this.editor.value=data.result.join("\n");
            this.editor.visible=true;
            this.editor.title="File name";

            this.removeTask(data.id);
        }));
    }
    write(order){
        let request=new WsMessage("writeOrder"+this.wss.requestCounter);
        request.method="fsctrl";
        request.params=order;
        this.wss.send(request);
        this.orderStack.push(new Task(request.id,(data)=>{
            this.update();
            this.removeTask(data.id);
        }))
    }
    dblclick(item:Path) {
        if(!this.isBlocked(item)){
            if (item.isdir == true) {
                this.fullPath.push(item);
                this.update();
            }
            if(item.type.indexOf("text")!=-1&&item.size<500000){
                let order = new Order();
                order.action="read";
                order.targets=[this.selected[0].inode];
                this.read(order);
                this.editor.action=(value)=>{
                    let order = new Order();
                    order.action="write";
                    order.text=value;
                    order.targets=[this.selected[0].inode];
                    this.write(order);
                };
            }
            if(item.type.indexOf("video")!=-1){
                item['added']=true;
                this.vs.addUniq([item]);
                this.video=true;
            }
            if(item.type.indexOf("image")!=-1){
                this.currentList.forEach(file=>{
                    if(file.type.indexOf("image")!=-1){
                        console.log(file);
                        this.imgModal.list.push(file)
                    }
                });
                this.imgModal.img=item;
                this.imgModal.visible=true;
            }


        }


    }
    toLevel(i){
        this.fullPath = this.fullPath.slice(0,i);
        this.update();
    }
    hasVideos(dbpaths:Path[]){
        if(_.find(dbpaths,(o)=>{return o.type.indexOf('video')!=-1})==null)
            return false;
        else return true;
    }
    sizeMask(size:number){
        if(size>1000000000){return (size/1000000000).toFixed(2)+"Gb"}
        else
        if(size>1000000){return (size/1000000).toFixed(2)+"Mb"}
        else
        if(size>1000){return (size/1000).toFixed(2)+"Kb"}
        else return size+"B"

    }
    iconSelect(item){
        if(item.type===undefined)return "fa-file";
        if(item.isdir==true)return "fa-folder";
        if(item.type.indexOf('image/')!=-1) return "fa-file-image-o";
        if(item.type.indexOf('video/')!=-1) return "fa-file-video-o";
        if(item.type.indexOf('audio/')!=-1) return "fa-file-audio-o";
        if(item.type.indexOf('text/')!=-1) return "fa-file-text-o";
        if(item.type.indexOf('msword')!=-1) return "fa-file-word-o";
        if(item.type.indexOf('ms-excel')!=-1) return "fa-file-excel-o";
        if(item.type.indexOf('pdf')!=-1) return "fa-file-pdf-o";
        if(item.type.indexOf('zip')!=-1||
            item.type.indexOf('tar')!=-1||
            item.type.indexOf('rar')!=-1||
            item.type.indexOf('7z')!=-1) return "fa-file-zip-o";
        if(item.isdir==false)return "fa-file";
    }
    dateMask(timestamp:number){
        let d = new Date();
        d.setTime(timestamp);
        let Day;
        let Month;
        if(d.getDay()<9)Day="0"+d.getDay();else Day=d.getDay();
        if(d.getMonth()<9)Month="0"+(d.getMonth()+1);else Month=d.getMonth();
        return `${Day}.${Month}.${d.getFullYear()}   ${d.getHours()}:${d.getMinutes()}`
    }

}
