/**
 * Created by zelenin on 21.09.16.
 */
import {Injectable, Inject} from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';
import {OutputService} from "./output.service";
import {Path} from "../model/Path";
import * as _ from 'lodash';
import {RootService} from "./root.service";
@Injectable()
export class VideoService{
    private url = `http://${this.rs.settings.server}`;
    constructor(private http: Http,@Inject(OutputService)private  stdout:OutputService,@Inject(RootService)private rs:RootService){}
    playlist:Path[]=[];
    getLink(dbpath:Path){
        return `${sessionStorage.getItem("protocol")}://${sessionStorage.getItem("server")}:8000/${dbpath.inode}`;
    }
    addUniq(items:Path[]){
        items.forEach(item=>{
            this.playlist.push(item);
        });
        this.playlist=_.uniqBy(this.playlist,'inode');
    }
    clearPlaylist(){
        this.playlist=[];
    }



}