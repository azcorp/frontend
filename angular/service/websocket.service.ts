/**
 * Created by zelenin on 23.09.16.
 */
import {Injectable, Inject} from "@angular/core";
import "rxjs/add/operator/toPromise";
import "rxjs/add/operator/catch";
import * as Rx from "rxjs/Rx";
import {OutputService} from "./output.service";
import {WsMessage} from "../model/WsMessage";


@Injectable()
export class WebSocketService{
    constructor(@Inject(OutputService)private outservice:OutputService){
       this.ws =<Rx.Subject<any>>this.connect(this.url).map((response: MessageEvent):WsMessage => {
           let data = JSON.parse(response.data);
           return data as WsMessage;
       });
       this.ObservableWs = this.ws.asObservable();
    }
    private ws;
    public ObservableWs;
    public isConnected = false;
    requestCounter:number=0;
    private subject: Rx.Subject<MessageEvent>;
    private url=`ws://${sessionStorage.getItem("server")}:8080/ws?access_token=${localStorage.getItem("token")}`;
    private connect(url): Rx.Subject<MessageEvent> {
        if(!this.subject) {
            this.subject = this.create(url);
        }
        return this.subject;
    }
    private create(url): Rx.Subject<MessageEvent> {
        let ws = new WebSocket(url);
        let observable = Rx.Observable.create((obs: Rx.Observer<MessageEvent>) => {
            ws.onopen = ()=>{this.isConnected=true;console.log("WebSocket open")};
            ws.onmessage = obs.next.bind(obs);
            ws.onerror = obs.error.bind(obs);
            ws.onclose = obs.complete.bind(obs);
            return ws.close.bind(ws);
        }).share();

        let observer = {
            next: (data: WsMessage) => {
                console.log(data,"send to",this.url);
                if (ws.readyState === WebSocket.OPEN) {
                    ws.send(JSON.stringify(data));
                }
            },
            error: (error)=>{
                this.isConnected=false;
                console.log("WebSocket error");
                this.outservice.addItem({style:"danger",message:error,title:"WebSocket Error"});
            },
            complete: ()=>{
                this.isConnected=false;
                console.log("WebSocket close");
                this.outservice.addItem({style:"warning",message:"",title:"WebSocket Disconnected"});
            }

        };

        return Rx.Subject.create(observer, observable);
    }
    public send(msg:WsMessage){
        this.requestCounter++;
        this.ws.next(msg)
    }

}