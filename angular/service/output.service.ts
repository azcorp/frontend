/**
 * Created by zelenin on 31.08.16.
 */
import { Injectable } from '@angular/core';
import * as _ from 'lodash';

@Injectable()
export class OutputService{
    private items=[];

    public getItems(){
        return this.items
    }
    deleteItem(item){
        _.remove(this.items,item)
    }
    public  addItem(item){
        this.items.push(item);
        console.log(this.items);
    }
    public clearAll(){
        _.drop(this.items,this.items.length)
    }

}