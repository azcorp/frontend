/**
 * Created by zelenin on 03.10.16.
 */
import {Injectable} from "@angular/core";
import {Headers} from "@angular/http";
import {WebSocketService} from "./websocket.service";
import {WsMessage, Task, Query} from "../model/WsMessage";


@Injectable()
export class RootService{
    constructor(private wss:WebSocketService) {
        this.wss.ObservableWs.subscribe((data)=>{this.receive(data)});
        this.headers = new Headers({
            'Content-Type': 'application/json',
            'Accept': 'application/json;q=0.9,*/*;q=0.8',
            'Authorization':'Bearer '+localStorage.getItem("token")
        });
    }
    public headers;
    public settings:Settings=JSON.parse(sessionStorage.getItem("settings"));
    public profile=JSON.parse(sessionStorage.getItem("profile"));
    iam:Task=new Task("who",()=>{});
    receive(data){
        this.iam.act(data);
    }

    update(){
        let request=new WsMessage("who"+this.wss.requestCounter);
        request.method="who";
        request.params=null;
        this.iam=new Task(request.id,(data)=>{
            this.profile=data.result;
            console.log(this.profile)
        });
        this.wss.send(request);
    }
    save(object,type){
        let request=new WsMessage("save"+this.wss.requestCounter);
        request.method="save";
        let query = new Query(type);
        query.save=[];
        if(object instanceof Array)
            object.forEach(o=>{
            query.save.push(o)
        });
        else
            query.save.push(object);
        request.params=query;
        this.wss.send(request);

    }

}
export class Settings{
    constructor(server){
        this.server=server;
    }
    server:string;
    vault:string;
    streamer:string;
}
