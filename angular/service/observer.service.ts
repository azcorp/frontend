/**
 * Created by zelenin on 29.09.16.
 */
import { Injectable} from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';
import {Observable, Observer} from 'rxjs/Rx';


@Injectable()
export class ObserverService{
    constructor(){
        this.ObservableScope={};
        this.ObserverScope={};
    }
    ObservableScope;
    ObserverScope;

    public getValue(name:string){
        return this.ObservableScope[name];
    }
    public addObservableValue(name:string){
        this.ObservableScope[name]=Observable.create(observer=>{this.ObserverScope[name]=observer;}).share();
    }
    public setValue(name:string,value:any){
        this.ObservableScope[name]=value;
        this.ObserverScope[name].next(this.ObservableScope[name])
    }
    public deleteObservableValue(name:string){
        delete this.ObservableScope[name];
        delete this.ObserverScope[name];
    }
}