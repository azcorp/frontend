/**
 * Created by zelenin on 02.11.16.
 */
import {NgModule} from "@angular/core";
import {Routes, RouterModule} from "@angular/router";
import {FMComponent} from "./component/fm.component";
import {HomeComponent} from "./component/home.component";
const routes: Routes =[
    {path: '',redirectTo: 'home',pathMatch: 'full'},
    {path:'home',component:HomeComponent},
    {path:'fm',component:FMComponent},
];
@NgModule({
    imports:[ RouterModule.forRoot(routes)],
    exports: [ RouterModule ],
})
export class RoutingModule{}