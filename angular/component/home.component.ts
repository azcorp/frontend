/**
 * Created by zelenin on 10.01.17.
 */
import {Component, OnInit} from "@angular/core";
import {RootService} from "../service/root.service";
import {WebSocketService} from "../service/websocket.service";

@Component({
    selector: 'home-component',
    templateUrl: '../templates/home.component.html'
})
export class HomeComponent implements OnInit {
    constructor(private rs:RootService,private wss:WebSocketService) { }
    ngOnInit() {
        new Promise((resolve)=>{
            let counter = setInterval(()=>{
                console.log("fm component wait");
                if(this.wss.isConnected){
                    resolve(true);
                    clearInterval(counter);
                }
            },1);
        }).then(()=>{
            this.rs.update();
        });
    }
}