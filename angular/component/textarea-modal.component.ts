/**
 * Created by zelenin on 10.01.17.
 */
import {Component, OnInit, Renderer, ViewChild, ElementRef, Input} from '@angular/core';

@Component({
    selector: 'textarea-modal-component',
    template: `
<div  class="overlay">
<div  class="callout small modal bigmodal">
<h6>{{editor.title}}</h6>
<textarea #in  [(ngModel)]="editor.value" autofocus></textarea>
<div class="controls">
<button (click)="ok(editor.value)" class="button hollow primary">Ok</button>
<button (click)="cancel()" class="button hollow primary">Cancel</button>
</div>
</div>
</div>
`,
    styleUrls: ['../style/modal.component.scss']
})
export class TextAreaModalComponent implements OnInit {
    constructor(private renderer: Renderer) {}
    @ViewChild('in') input: ElementRef;
    ngOnInit(): void {

    }
    @Input() editor;
    enter(event,value){
        if(event.key=="Enter"){
            this.ok(value)
        }
    }    cancel(){
    this.editor.value=null;
    this.editor.visible=false
    }
    ok(value){
        this.editor.action(value);
        this.editor.visible=false
    }
    ngAfterViewInit() {
        this.renderer.invokeElementMethod(this.input.nativeElement, 'focus');
    }
}