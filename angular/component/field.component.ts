/**
 * Created by zelenin on 13.01.17.
 */
import {Component, OnInit, Input} from '@angular/core';

@Component({
    selector: 'field-component',
    template: `

<div *ngIf="!editeble" class="callout small">
<span>{{field}}</span><span><button (click)="editeble=!editeble" class="fa fa-edit"></button></span>
</div>
<div *ngIf="editeble" class="input-group">
  <span class="input-group-label">$</span>
  <input class="input-group-field" type="text" style="width: 50%" [(ngModel)]="field"/>
  
  <div class="input-group-button">
    <input type="submit" (click)="submit()" class="button" value="Submit">
  </div>
</div>

`,
    styleUrls: ['../style/field.component.scss']
})
export class FieldComponent implements OnInit {
    constructor() { }
    ngOnInit() { }

    @Input()
    field:any;
    @Input()
    editeble:boolean=false;
    submit(){
        this.editeble=!this.editeble;
       // this.field.method();
    }



}