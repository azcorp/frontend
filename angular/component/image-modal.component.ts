/**
 * Created by zelenin on 14.01.17.
 */
import {Component, OnInit, Input} from '@angular/core';
import * as _ from 'lodash';
import {Path} from "../model/Path";
import {FileManagerService} from "../service/fm.service";
@Component({
    selector: 'image-modal-component',
    templateUrl: '../templates/image-modal.component.html',
    styleUrls: ['../style/modal.component.scss']
})
export class ImageModalComponent implements OnInit {
    constructor(private fms:FileManagerService) { }
    ngOnInit() {

    }

    @Input()
        imgModal;


    next(){
        let index =_.findIndex(this.imgModal.list,o=>{return o['inode']==this.imgModal.img.inode;});
        if(index<this.imgModal.list.length-1){
            this.imgModal.img = this.imgModal.list[index+1]
        }
    }
    previous(){
        let index =_.findIndex(this.imgModal.list,o=>{return o['inode']==this.imgModal.img.inode;});
        if(index>0){
            this.imgModal.img = this.imgModal.list[index-1]
        }
    }
    close(){
        this.imgModal.visible=false
    }



}