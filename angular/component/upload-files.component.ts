/**
 * Created by zelenin on 17.11.16.
 */
import { Component, OnInit } from '@angular/core';
import {FileManagerService} from "../service/fm.service";

@Component({
    selector: 'upload-files-component',
    template: `
<button class="dropdown-item button hollow primary expanded uploadbutton" >
  <label class="custom-file uploadlabel">
      <input class="custom-file-input" id="uploader" type="file" name="files" data-url="fs/upload" multiple style="display: none;"/>
      <span class="custom-file-control uploader"><i class="fa fa-cloud-upload uploadtext">Upload</i></span>
  </label>
</button>`,
    styleUrls: ['../style/upload-files.comonent.scss']
})
export class UploadFilesComponent implements OnInit {
    constructor(){}
    ngOnInit() { }
}