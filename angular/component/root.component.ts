/**
 * Created by zelenin on 26.08.16.
 */
import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {FileManagerService} from "../service/fm.service";
import {VideoService} from "../service/video.service";
import {RootService} from "../service/root.service";
import {WebSocketService} from "../service/websocket.service";
require('../style/root.comonent.scss');
@Component({
    selector: 'root-component',
    template: `
    <div class="root-component">
    <nav-component>
    <i class="fa fa-spinner fa-cog fa-5x fa-fw"></i><span class="sr-only">Loading...</span>
    </nav-component>
    <output-component>
    <i class="fa fa-spinner fa-cog fa-5x fa-fw"></i><span class="sr-only">Loading...</span>
    </output-component>
    <div class="outlet">
    <router-outlet ></router-outlet>
    </div>
    </div>`,
    styleUrls: [ '../style/root.comonent.scss' ],
    providers: [ RootService,FileManagerService, VideoService ],
    encapsulation: ViewEncapsulation.None,
})
export class  RootComponent implements OnInit{
    constructor(){

    }
    ngOnInit(): void {
    }



}
