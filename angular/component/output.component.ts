/**
 * Created by zelenin on 31.08.16.
 */
import {Component, OnInit} from "@angular/core";
import {OutputService} from "../service/output.service";


@Component({
    selector: 'output-component',
    template:
`<div class="output-component">
    <div *ngFor="let item of items"  class="callout {{item?.style}}">
  <button (click)="deleteItem(item)" class="close" type="button" aria-label="Close">
  <span  aria-hidden="true">&times;</span>
  </button>
  <strong>{{item?.title}}</strong>
  <div>{{item?.message}}</div> 
</div>
</div>`
})

export class OutputComponet implements OnInit{
    items=[];
    constructor(public output:OutputService){}
    ngOnInit(): void {
        this.items = this.output.getItems();
    }
    deleteItem(item){
        this.output.deleteItem(item)
    }



}