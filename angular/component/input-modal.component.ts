/**
 * Created by zelenin on 14.11.16.
 */
import {Component, ElementRef, Inject, Input, OnInit, Renderer, ViewChild, AfterViewInit} from '@angular/core';

@Component({
    selector:'input-modal-component',
    template:`
<div  class="overlay">
<div  class="callout small modal">
<h6>{{modal.title}}</h6>
<input #in type="text" (keyup)="enter($event,modal.value)" [(ngModel)]="modal.value" autofocus/>
<div class="controls">
<button (click)="ok(modal.value)" class="button hollow primary">Ok</button>
<button (click)="cancel()" class="button hollow primary">Cancel</button>
</div>
</div>
</div>`,
    styleUrls: ['../style/modal.component.scss']
})
export class InputModalComponent implements OnInit,AfterViewInit{
    constructor(private renderer: Renderer) {}
    @ViewChild('in') input: ElementRef;
    ngOnInit(): void {

    }
    @Input() modal;
    enter(event,value){
        if(event.key=="Enter"){
            this.ok(value)
        }
    }    cancel(){
        this.modal.value=null;
        this.modal.visible=false
    }
    ok(value){
        this.modal.action(value);
        this.modal.visible=false
    }
    ngAfterViewInit() {
        this.renderer.invokeElementMethod(this.input.nativeElement, 'focus');
    }
}