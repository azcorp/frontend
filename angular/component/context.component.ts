/**
 * Created by zelenin on 02.11.16.
 */
import {Component, OnInit, Input} from '@angular/core';


@Component({
    selector: 'context-component',
    template: `
<div>
<div *ngFor="let item of context.items" (click)="item.func()">{{item.name}}</div>
</div>
`
})
export class ContextComponent{
@Input() context;

}