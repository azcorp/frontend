/**
 * Created by zelenin on 28.09.16.
 */
import {Component, OnInit, ElementRef, ViewChild, ViewChildren} from "@angular/core";
import {WebSocketService} from "../service/websocket.service";
import {VideoService} from "../service/video.service";
import * as _ from "lodash";
import {WsMessage, Query, Task} from "../model/WsMessage";
import {Path} from "../model/Path";
import {Profile} from "../model/Profile";
import {FileManagerService} from "../service/fm.service";
@Component({
    selector:"search-component",
    templateUrl: "../templates/search.component.html",
    styleUrls: ['../style/search.comonent.scss','../style/modal.component.scss'],
    host: {
        '(document:click)': 'onClick($event)',
    }
})
export class SearchComponent implements OnInit{
    constructor(
        private wss:WebSocketService,
        private vs:VideoService,
        private fms:FileManagerService
    ){}
    @ViewChild('input') ElementInput:ElementRef;
    @ViewChildren('list') ElementList:ElementRef;
    @ViewChild('container') container:ElementRef;
    ngOnInit(): void {
        this.ListStyle.Width=this.ElementInput.nativeElement.offsetWidth;
        this.ListStyle.Left=this.ElementInput.nativeElement.offsetLeft;
        this.InfoStyle.Width=this.ElementInput.nativeElement.offsetWidth;
        this.wss.ObservableWs.subscribe((data)=>{this.recieve(data)});
    }
    requestCounter:number=0;
    ListStyle={Top:0, Left:0, Width:0};
    InfoStyle={Top:0, Left:0, Width:0};
    SearchResult:any[]=[];
    isAutoOpen=false;
    isSettingsOpen=false;
    block:Task;
    detail=null;
    SearchQuery='';
    types=[{value:'path',name:'File'},{value:'profile',name:'Profile'}];
    type='path';
    limit=20;
    offset=0;
    field='name';
    selectedIndex=0;
    recieve(data){
        if(this.block!=null&&data.result!=null&&data.result instanceof Array){
            this.block.act(data);
        }
    }
    openAuto(){
        if((this.SearchResult.length>0||this.block)&&this.SearchQuery.length>0){
            this.isAutoOpen=true;
        }
        else{
            this.closeInfo();
            this.isAutoOpen=false;
        }
        if(this.SearchQuery.length==0){
            this.SearchResult=[];
        }

    }
    closeAuto(){
        this.isAutoOpen=false;
    }
    keyListener(event){
        switch (event.code){
            case "ArrowDown":
                if(this.SearchResult.length>0&&this.selectedIndex<this.SearchResult.length-1){
                    this.selectedIndex++;

                }
                if(this.SearchResult.length>0){
                    this.ElementList['_results'][this.selectedIndex].nativeElement.click();
                }
                break;
            case "ArrowUp":
                if(this.SearchResult.length>0&&this.selectedIndex>0){
                    this.selectedIndex--;

                }
                if(this.SearchResult.length>0){
                    this.ElementList['_results'][this.selectedIndex].nativeElement.click();
                }
                break;
            case "ArrowLeft":

                break;
            case "Enter":
                this.sendRequest();
                break;
        }
    }
    sendRequest=_.debounce(()=>{
        let request = new WsMessage("search"+this.requestCounter);
        request.method="find";
        let query =new Query(this.type);
        //query.sort=[{direct:"ASC",val:"isdir"},{direct:"ASC",val:"text"}];
        if(this.isSettingsOpen)this.isSettingsOpen=false;
        query.likes = [{cr:this.field,val:this.screening(this.SearchQuery)}];
        query.type= this.type;
        query.limit=this.limit;
        query.offset=this.offset;
        request.params=query;

        this.block=new Task(request.id,(data)=>{
            this.SearchResult=data.result;
        });console.log(this.block);
        if(this.block.block&&this.SearchQuery.length>=2){
            this.wss.send(request);
        }
        this.openAuto();
    },300);
    getFields(){
        switch (this.type){
            case "path":
                return Object.getOwnPropertyNames(new Path(0,"name"));
            case "profile":
                return Object.getOwnPropertyNames(new Profile());
            default:
                return null;

        }
    }
    openSettings(){
        if(this.isAutoOpen){this.closeAuto(); console.log("close auto")}
        else if((!this.isAutoOpen)&&this.SearchResult.length>0)
        {this.openAuto();console.log("open auto")}

        this.ListStyle.Width=this.ElementInput.nativeElement.offsetWidth;
        this.ListStyle.Left=this.ElementInput.nativeElement.offsetLeft;
        this.isSettingsOpen=!this.isSettingsOpen;
    }
    closeSettings(){
        this.isSettingsOpen=false;
    }
    closeInfo(){this.detail=null;}
    screening(string){
        // let pattern =/[\[\]\\{\\}\\.\^\\$\\|\\?\\*\\+\\(\\)]/g;
        let pattern =/[%_]/g;
        return `${string.replace(pattern, "\\$&")}%`;
    }
    openInfo(item,event,index){
        this.detail = item;
        this.selectedIndex=index;
        this.InfoStyle.Top=event.target.offsetTop+event.target.offsetHeight;
        this.InfoStyle.Left=this.ElementInput.nativeElement.offsetLeft-this.ElementInput.nativeElement.offsetWidth;
    }
    isPath(item){
        return undefined !=item.inode;
    }
    isProfile(item){
        return undefined !=item.username;
    }

    onClick(event:any) {
        if (this.detail!=null&&!this.container.nativeElement.contains(event.target)) {
            this.isAutoOpen=false;
            this.closeInfo()
        }
    }





}