import {Component, OnInit, Input, OnChanges, SimpleChanges, OnDestroy} from '@angular/core';
import {FileManagerService} from "../service/fm.service";
import {Path} from "../model/Path";
import {VideoService} from "../service/video.service";
import {RootService} from "../service/root.service";


@Component({
    selector: 'file-info-component',
    template: `
<div>
 <div class="field">
     <b>Filename:</b><br>
     {{item?.name}}<br>
 </div>
 <div class="field">
     <b>Type:</b><br>
     {{item?.type}}<br>
 </div>
 <div class="field">
     <b>CreationTime:</b><br>
     {{fms.dateMask(item?.createtime)}}<br>
 </div>
 <div class="field">
     <b>LastModTime:</b><br>
     {{fms.dateMask(item?.lastmodtime)}}<br>
 </div>
 <div class="field">
     <b>Owner:</b><br>
     {{item?.owner}}<br>
 </div>
 <div class="field">
     <b>Size:</b><br>
     {{fms.sizeMask(item?.size)}}<br>
 </div>
</div>
<div class="stacked-for-small button-group" >
<button *ngIf="!item?.isdir" type="button" (click)="fms.download(item)" class="button hollow primary">
      <i class="fa fa-download text">Download</i>
</button>
<button *ngIf="!item?.isdir" type="button" (click)="link=!link" class="button hollow primary">
      <i class="fa fa-share-alt text">Share link</i>
</button>
<button *ngIf="!item?.isdir" type="button" (click)="tags=!tags" class="button hollow primary">
      <i class="fa fa-tags text">Tags editor</i>
</button>
<button *ngIf="!item?.isdir&&fms.hasVideos([item])" type="button" (click)="open(item)" class="button hollow primary">
      <i class="fa fa-play-circle-o text">Play</i>
</button>
<button *ngIf="item.type.indexOf('text/')!=-1&&item.size<5000000" type="button" (click)="open(item)" class="button hollow primary">
      <i class="fa fa-edit text">Edit</i>
</button>
<button *ngIf="item.type.indexOf('image/')!=-1" type="button" (click)="view(item)" class="button hollow primary">
      <i class="fa fa-image text">View</i>
</button>
</div>
  <div *ngIf="!item?.isdir&&link" class="small callout" id="link">
      <b>Link</b>
      <div class="input-group">
          <input class="input-group-field"  id="l"  value="{{fms.getLink(item)}}"  type="text"/>
          <span class="input-group-label" id="lcopy" data-clipboard-target="#l"><i class="fa fa-clipboard"></i></span>
      </div>
  </div>
  <div *ngIf="!item?.isdir&&tags" class="small callout" id="tags">
      <b>Tags Editor</b>
      <div >
     <span *ngFor="let tag of tagsCollection">
         <div  class="label tag">{{tag}}</div>
     </span>
      </div>
      <input (keydown.enter)="setTags(item,tagsString)" class="form-control-sm" placeholder="Write tags divided by this symbol ','" type="text" [(ngModel)]="tagsString" />
      <div class="tagctrl">
          <button   (click)="setTags(item,tagsString)">Set</button>
          <button   (click)="setTags(item,'')">Clear</button>
  </div>
</div>
`
})
export class FileInfoComponent implements OnInit,OnChanges {

    ngOnChanges(changes: SimpleChanges): void {
        if(this.item.tags!==''&&this.item['tags']!==undefined)  this.tagsCollection = this.item.tags.split(','); else this.tagsCollection =[];
    }
    constructor(private fms:FileManagerService,private vs:VideoService,private rs:RootService) {}
    ngOnInit() {
        if(this.item.tags!==''&&this.item['tags']!==undefined)  this.tagsCollection = this.item.tags.split(',');else this.tagsCollection =[];

    }
    tagsCollection=[];
    tags=false;
    link=false;
    tagsString="";
    @Input() item:Path;
    setTags(item,string){
        item.tags= string;
        if(this.item.tags.split(',').length>0){
            this.tagsCollection = this.item.tags.split(',');
            this.tagsString="";
        }
        else {
            this.tagsCollection = [];
            this.tagsString="";
        }

        this.rs.save(item,"path");
    }
    open(item){
       this.fms.dblclick(item);
    }
    view(item){
        this.fms.imgModal.list.push(item);
        this.fms.imgModal.img=item;
        this.fms.imgModal.visible=true;
    }


}