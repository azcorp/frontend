/**
 * Created by zelenin on 14.11.16.
 */
import {Component, Input} from '@angular/core';
import {FileManagerService} from "../service/fm.service";
@Component({
    selector:'file-line-component',
    template:`
 <div (dblclick)="fms.dblclick(item)"
      [class.selected]="fms.isSelectedContain(item)"
      [class.ghost]="fms.isBlocked(item)"
      class="small callout row expanded">
      <div class="small-5 medium-5 large-5 columns icon-name">
          <div class="icon fa {{fms.iconSelect(item)}} icon-name"></div>
          <div class="field unselected" style="margin-left: 0.5rem" data-toggle="tooltip" title="{{item?.name}}">
              {{item?.name}}
          </div>
      </div>
      <div class="small-4 medium-4 large-4 columns">
          <div class="field unselected" data-toggle="tooltip" title="{{item?.type}}">
              {{item?.type}}
          </div>
      </div>
      <div class="small-3 medium-3 large-3 columns">
          <div class="field unselected" *ngIf="item?.isdir==0">
              {{fms.sizeMask(item?.size)}}
          </div>
      </div>
</div>`,
    styleUrls: ['../style/file-line.component.scss']
})
export class FileLineComponent{
    constructor(private fms:FileManagerService){}
    @Input() item;
}
