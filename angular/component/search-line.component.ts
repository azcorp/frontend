/**
 * Created by zelenin on 10.01.17.
 */
import {Component, OnInit, Input, ViewChild, ElementRef} from '@angular/core';
import {Path} from "../model/Path";
import {isUndefined} from "util";
import {FileManagerService} from "../service/fm.service";

@Component({
    selector: 'search-line',
    template: `
<div class="field">
    <span *ngIf="item?.inode" class="fa {{fms.iconSelect(item)}}" style="margin-right: 0.2rem"></span><span *ngIf="item?.inode"  >{{item?.name}}</span>
    <span *ngIf="item?.username"  class="warning callout small cursor-default column text-center" style="margin-right: 0.4rem; border-radius: 50%">{{getInitials(item)}}</span><span *ngIf="item?.username" style="font-weight: bold" >{{item?.username}}</span>
</div>
`
})
export class SearchLineComponent implements OnInit {
    constructor(private fms:FileManagerService) {}
    ngOnInit() {}
    @Input() item;
    getInitials(profile){
        return (profile.name.charAt(0)+profile.surname.charAt(0)).toUpperCase()
    }

}