/**
 * Created by zelenin on 31.08.16.
 */
import {Component, OnInit, ViewChild, ElementRef} from "@angular/core";
import {FileManagerService} from "../service/fm.service";
import {OutputService} from "../service/output.service";
import {VideoService} from "../service/video.service";
import {ObserverService} from "../service/observer.service";
import {Router} from "@angular/router";
import {WebSocketService} from "../service/websocket.service";
import {Order} from "../model/WsMessage";
import {RootService} from "../service/root.service";

@Component({
    selector:'fm-component',
    templateUrl:'../templates/fm.component.html',
    styleUrls: ['../style/fm-component.scss']
})

export class FMComponent implements OnInit{
    constructor(
        private fms:FileManagerService,
        private vs:VideoService,
        private observers:ObserverService,
        private wss:WebSocketService,
        private rs:RootService){
        this.clipboard = require('clipboard');
        this.clipboard = new this.clipboard('#lcopy');
        this.observers.addObservableValue("upload-progress");
    }
    @ViewChild('uploader') uploader: ElementRef;
    ngOnInit(): void {
        new Promise((resolve)=>{
            let counter = setInterval(()=>{
                console.log("fm component wait");
                if(this.wss.isConnected){
                    resolve(true);
                    clearInterval(counter);
                }
            },1);
        }).then(()=>{
            this.fms.update();
        });
    }
    modal={visible:false,title:"Enter text here",value:"",action: null};
    order = new Order();
    menu=[
        {name: 'Play',
            icon: 'fa fa-play',
            condition: ()=>{return this.fms.selected.length>0&&this.fms.hasVideos(this.fms.selected)},
            callout: ()=>{
                this.fms.selected.forEach((item)=>{
                    item['added']=true;
                });
                this.vs.addUniq(this.fms.selected);
                this.fms.video=true;
            }
        }, {name: 'Edit',
            icon: 'fa fa-edit',
            condition: ()=>{return this.fms.selected.length==1&&this.fms.selected[0].type.indexOf('text/')!=-1&&this.fms.selected[0].size<5000000},
            callout: ()=>{
                this.fms.dblclick(this.fms.selected[0])
            }
        },
        {name: 'View',
            icon: 'fa fa-image',
            condition: ()=>{return this.fms.selected.length==1&&this.fms.selected[0].type.indexOf('image/')!=-1},
            callout: ()=>{
                this.fms.dblclick(this.fms.selected[0])
            }
        },
        {name: 'Copy',
            icon: 'fa fa-copy',
            condition: ()=>{return this.fms.selected.length>0},
            callout: ()=>{
                this.fms.setBuffer(this.fms.selected,false);
                this.order.action="copy"
            }
        },
        {name: 'Cut',
            icon: 'fa fa-cut',
            condition: ()=>{return this.fms.selected.length>0},
            callout: ()=>{
                this.fms.setBuffer(this.fms.selected,true);
                this.order.action="move"
            }
        },
        {name: 'Create directory',
            icon: 'fa fa-folder',
            condition: ()=>{return true},
            callout: ()=>{
                this.modal.visible=true;
                this.modal.value="";
                this.modal.title="Enter new directory text ...";
                this.modal.action=(value)=>{
                   this.order.action="mkdir";
                   this.order.text=value;
                   this.order.targets=[this.fms.fullPath[this.fms.fullPath.length-1].inode];
                    this.fms.action(this.order);
                    this.order=new Order();
                };
            }
        },
        {name: 'Create file',
            icon: 'fa fa-file',
            condition: ()=>{return true},
            callout: ()=>{
                this.modal.visible=true;
                this.modal.value="";
                this.modal.title="Enter new directory text ...";
                this.modal.action=(value)=>{
                    this.order.action="mkfile";
                    this.order.text=value;
                    this.order.targets=[this.fms.fullPath[this.fms.fullPath.length-1].inode];
                    this.fms.action(this.order);
                    this.order=new Order();
                };
            }
        },
        {name: 'Rename',
            icon: 'fa fa-pencil',
            condition: ()=>{return this.fms.selected.length==1},
            callout: ()=>{
                this.modal.visible=true;
                this.modal.value=this.fms.selected[0].name;
                this.modal.title="Enter new text ...";
                this.modal.action=(value)=>{
                    this.order.action="rename";
                    this.order.text=value;
                    this.fms.selected.forEach(path=>{
                        this.order.targets=[path.inode]
                    });
                    this.fms.action(this.order);
                    this.order=new Order();
                };
            }
        },
        {name: 'Past with overwrite',
            icon: 'fa fa-paste',
            condition: ()=>{return this.fms.buffer.list.length>0},
            callout: ()=>{
                this.order.sources=[];
                this.fms.buffer.list.forEach((path)=>{
                    this.order.sources.push(path.inode);
                });
                this.order.targets=[this.fms.fullPath[this.fms.fullPath.length-1].inode];
                this.order.overwrite=true;
                this.fms.action(this.order);
                this.order=new Order();
                this.fms.clearBuffer()
            }
        },
        {name: 'Past with skip',
            icon: 'fa fa-paste',
            condition: ()=>{return this.fms.buffer.list.length>0},
            callout: ()=>{
                this.order.sources=[];
                this.fms.buffer.list.forEach((path)=>{
                    this.order.sources.push(path.inode);
                });
                this.order.targets=[this.fms.fullPath[this.fms.fullPath.length-1].inode];
                this.order.overwrite=false;
                this.fms.action(this.order);
                this.order=new Order();
                this.fms.clearBuffer()
            }
        },
        {name: 'Delete',
            icon: 'fa fa-trash',
            condition: ()=>{return this.fms.selected.length>0},
            callout: ()=>{
                this.order.action="delete";
                this.order.targets=[];
                this.fms.selected.forEach(path=>{
                    this.order.targets.push(path.inode)
                });
                this.fms.action(this.order);
                this.order=new Order();
            }
        },
        {name: 'Upload files',
            icon: 'fa fa-upload',
            condition: ()=>{return true},
            callout: ()=>{
            this.uploader.nativeElement.click();
            }
        },
    ];
    clipboard;
    progress;



}
