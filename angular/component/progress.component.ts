/**
 * Created by zelenin on 19.09.16.
 */
import {Component, ChangeDetectorRef, Input} from '@angular/core';
import {FileManagerService} from "../service/fm.service";
import {ObserverService} from "../service/observer.service";
@Component({
   selector:"progress-bar",
   template:`
<div *ngIf="progress>0" class="callout small">
<h6>Load {{progress}} %</h6>
<div  class="progress" role="progressbar">
  <span class="progress-meter" [style.width]="progress+'%'"></span>
</div>
</div>
`
})
export class ProgressBarComponent{
   constructor(private ref:ChangeDetectorRef,private observers:ObserverService){
       this.observers.getValue("upload-progress").subscribe((data)=>{
           this.progress=data;
           this.ref.detectChanges();
       })
   }
   @Input() progress;
}