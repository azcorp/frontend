/**
 * Created by zelenin on 17.11.16.
 */
import {Component, OnInit, Input} from '@angular/core';
import {FileManagerService} from "../service/fm.service";

@Component({
    selector: 'file-plate-component',
    template: `<div class="item"
                 (click)="fms.selectItem($event,item)"
                 (dblclick)="fms.dblclick(item)"
                 [class.selected]="fms.isSelectedContain(item)"
                 [class.ghost]="item['ghost']">
                <div class="fa {{fms.iconSelect(item)}} fa-4x "></div>
                <div class="field unselected" style="text-align: center;">{{item?.name}}</div>
            </div>`,
    styleUrls: ['../style/file-plate.component.scss']
})
export class FilePlateComponent implements OnInit {
    constructor(private fms:FileManagerService){}
    @Input() item;
    ngOnInit() { }

}