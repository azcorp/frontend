/**
 * Created by zelenin on 28.08.16.
 */
import {Component, OnInit} from "@angular/core";
import {Router, NavigationEnd} from "@angular/router";

@Component({
    selector: 'nav-component',
    templateUrl: '../templates/nav.component.html',
    styleUrls: ['../style/nav.component.scss']
})

export class NavComponet implements OnInit{
    constructor(private router:Router) {
       router.events.subscribe((event)=>{
          if(event instanceof NavigationEnd){
              switch (event.urlAfterRedirects) {
                  case "/home":
                      this.currentPosition="Home";
                      this.currentIcon="fa fa-user-circle";
                      break;
                  case "/fm":
                      this.currentPosition="File Storage";
                      this.currentIcon="fa fa-folder-open-o";
                      break;
              }
          }
       })
    }
    currentPosition;
    currentIcon;
    ngOnInit(): void { }
    menu = [
        {
            name: "Home",
            icon: "fa fa-user-circle",
            condition: () => {return true},
            callout: ()=>{this.router.navigate(['/home'])}
        },
       {
           name:"File Storage",
           icon:"fa fa-folder-open-o",
           condition: ()=>{return true},
           callout: ()=>{this.router.navigate(['/fm'])}
       }
       ];


}