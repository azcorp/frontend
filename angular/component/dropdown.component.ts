/**
 * Created by zelenin on 18.11.16.
 */
import {Component, OnInit, Input, ViewChild, ElementRef} from '@angular/core';
import {FileManagerService} from "../service/fm.service";

@Component({
    selector: 'dropdown-component',
    template: `
<div #container>
    <button  class="button primary hollow expanded unselected {{icon}}" (click)="OpenMenu($event)" >
        {{title}}
    </button>
    <div *ngIf="isMenuOpen" class="angular-dropdown"
        [style.top]="MenuStyle?.MenuTop"
        [style.left]="MenuStyle?.MenuLeft"
        [style.width]="MenuStyle?.MenuWidth">
        <div *ngFor="let item of menu">
        <button 
        *ngIf="item.condition()"
        class="dropdown-item  primary hollow  button  expanded"
        (click)="OpenMenu($event);item.callout($event);">
                <i style="margin-right:2px" class="{{item.icon}}"></i>
                {{item.name}}
        </button>
        </div>
    </div>
</div>`,
    styleUrls: ['../style/dropdown.component.scss'],
    host: {
        '(document:click)': 'onClick($event)',
    }
})
export class DropdownComponent implements OnInit {
    constructor(){
    }
    ngOnInit() {}
    @ViewChild('container') container:ElementRef;
    MenuStyle={MenuTop:0, MenuLeft:0, MenuWidth:0};
    isMenuOpen=false;
    OpenMenu(event){
        this.isMenuOpen=!this.isMenuOpen;
        this.MenuStyle={MenuTop:event.target.offsetHeight+event.target.offsetTop, MenuLeft:event.target.offsetLeft, MenuWidth:event.target.offsetWidth};
        console.log(this.isMenuOpen);
        console.log("open",this.container)
    }
    @Input() menu;
    @Input() icon;
    @Input() title;

    onClick(event:any) {
        if (this.isMenuOpen&&!this.container.nativeElement.contains(event.target)) {
            this.isMenuOpen=false;
        }
    }

}