/**
 * Created by zelenin on 31.08.16.
 */
import {Component, OnInit, OnChanges, Input, SimpleChanges} from "@angular/core";
import {Profile} from "../model/Profile";
import {FileManagerService} from "../service/fm.service";
import {RootService} from "../service/root.service";
//var $ = require('jquery');
//var foundation = require('foundation-sites/js/foundation.core');
//$.fn.foundation =foundation.foundation;
//$(this.el.nativeElement.ownerDocument).foundation();
@Component({
    selector:'profile-component',
    templateUrl: '../templates/profile.component.html',
    styleUrls: ['../style/profile.comonent.scss']
})
export class ProfileComponent implements OnInit,OnChanges{
    constructor(private fms:FileManagerService,private rs:RootService){}
    ngOnChanges(changes: SimpleChanges): void {
        this.initials=(this.profile.name.charAt(0)+this.profile.surname.charAt(0)).toUpperCase();
        this.volume=Math.round(this.profile.engaged / this.profile.quota * 100);
    }
    ngOnInit() {
       this.initials=(this.profile.name.charAt(0)+this.profile.surname.charAt(0)).toUpperCase();
       this.volume=Math.round(this.profile.engaged / this.profile.quota * 100);
    }
    @Input()
    profile:Profile;
    @Input()
    showtools:boolean;
    initials;
    editeble:boolean=false;
    volume:number;
    modalInput={visible:false,title:"",value:"",action:null};

    logout(){
        localStorage.removeItem("auth");
        localStorage.removeItem("token");
        window.location.href="/login.html";
    }
    edit(){
        this.editeble=!this.editeble;
    }
    save(profile){

    }
    editName(){
        this.modalInput.action=(value)=>{
            this.profile.name=value;
            this.rs.save(this.profile,"profile")
        };
        this.modalInput.title="Enter your name...";
        this.modalInput.value=this.profile.name;
        this.modalInput.visible=true;

    }
    editSurname(){
        this.modalInput.action=(value)=>{
            this.profile.surname=value;
            this.rs.save(this.profile,"profile")
        };
        this.modalInput.title="Enter your surname...";
        this.modalInput.value=this.profile.surname;
        this.modalInput.visible=true;
    }
    editAvatar(){
        this.modalInput.action=(value)=>{
            this.profile.avatar=value;
            this.rs.save(this.profile,"profile")
        };
        this.modalInput.title="Enter link on your avatar...";
        this.modalInput.value=this.profile.avatar;
        this.modalInput.visible=true;
    }

}