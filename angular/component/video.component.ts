/**
 * Created by zelenin on 31.08.16.
 */
import {Component, OnInit, OnDestroy, Input} from '@angular/core';
import {Path} from "../model/Path";
import {VideoService} from "../service/video.service";
import * as _ from 'lodash';
import {FileManagerService} from "../service/fm.service";
const videojs=require('../../node_modules/video.js/');
@Component({
    selector:'video-component',
    templateUrl: '../templates/video.component.html',
    styleUrls: ['../style/video.component.scss','../style/modal.component.scss']
})
export class VideoComponent implements OnInit,OnDestroy{
    constructor(private vs:VideoService,private fms:FileManagerService){}
    player;
    @Input() visible;
    ngOnInit(): void {
        this.player = videojs('Player').ready(()=>{this.player.on("ended",()=>{this.next()})});
        _.remove(this.vs.playlist,(o)=>{if (o.type.indexOf("video")==-1) return true});
        this.vs.playlist=_.uniq(this.vs.playlist);
        if(this.vs.playlist.length>0){
            this.play(_.find(this.vs.playlist,(o)=>{return o['added']==true}))
        }
    }
    ngOnDestroy():void{
        this.player.dispose()
    }
    deleteFromPlaylist(dbpath:Path){
        if(dbpath['play'])this.next();
        _.remove(this.vs.playlist,dbpath);
        if(this.vs.playlist.length==0){
        this.reset();
        }
    }
    play(dbpath:Path){
        if(dbpath!=null){
            this.clearPlay();
            dbpath['play']=true;
            this.player.src({src:this.vs.getLink(dbpath),type:dbpath.type});
            this.player.play();
        }
    }
    next(){
        let index =_.findIndex(this.vs.playlist,function (o) {return o['play']==true});
        console.log(index);
        console.log(this.vs.playlist);
        if (index<=this.vs.playlist.length){
            console.log("play");
            this.play(this.vs.playlist[index+1])

        }else {
            console.log("reset");
            this.reset()
        }

    }
    reset(){
        this.player.reset();
    }
    stop(){
        this.player.stop();
    }
    clearPlay(){this.vs.playlist.forEach(p=>{p['play']=false;});}
    left(dbp:Path, dbplist:Path[]){
        let index = dbplist.indexOf(dbp);
        if((index-1)!=-1){
            let tmp;
            tmp=dbplist[index-1];
            dbplist[index-1]=dbplist[index];
            dbplist[index]=tmp;
        }

    }
    right(dbp:Path, dbplist:Path[]){
        let index = dbplist.indexOf(dbp);
        if((index+1)!=dbplist.length){
            let tmp;
            tmp=dbplist[index+1];
            dbplist[index+1]=dbplist[index];
            dbplist[index]=tmp;
        }
    }
    close(){
        this.fms.video=false;
    }

}