/**
 * Created by zelenin on 28.11.16.
 */
export class WsMessage{
    constructor(id){
        this.id = id;
        this.jsonrpc="2.0";
    }

    id:string;
    method:string;
    params:any;
    result:any;
    error:any;
    jsonrpc:string;
}
export class Query{
    constructor(type){
        this.type = type;
        this.limit = 500;
        this.offset = 0;
        this.ignoreCase = false;
    }
    likes;
    equals;
    sort:any[];
    type:string;
    limit:number;
    offset:number;
    ignoreCase:boolean;
    save:any;

}
export class Order{
    constructor(){
        this.sources=null;
        this.targets=null;
        this.overwrite=null;
        this.action=null;
        this.text=null;
    }
    sources:number[];
    targets:number[];
    text:string;
    overwrite:boolean;
    action:string;
}
export class Task{
    constructor(id,callback){
        this.id=id;
        this.block=true;
        this.callback=callback;
    }
    public id:number;
    public block:boolean;
    public callback;
    public act(data){
        if(this.id==data.id)
        {
            this.block=false;
            this.callback(data);
        }
    }
}