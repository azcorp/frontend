

export class Profile {
    constructor(){
        this.username=null;
        this.password=null;
        this.name=null;
        this.surname=null;
        this.email=null;
        this.link=null;
        this.quota=null;
        this.engaged=null;
        this.avatar=null;
        this.role=null;
        this.enabled=null;
    }
    username: string;
    password: string;
    name: string;
    surname:string;
    email:string;
    avatar:string;
    quota:number;
    engaged:number;
    link:number;
    role:string;
    enabled:boolean;

}
