/**
 * Created by zelenin on 26.08.16.
 */
import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {HttpModule} from "@angular/http";
import {FormsModule} from "@angular/forms";
import {RoutingModule} from "./routing.module";
import {OutputService} from "./service/output.service";
import {RootComponent} from "./component/root.component";
import {NavComponet} from "./component/nav.component";
import {OutputComponet} from "./component/output.component";
import {FMComponent} from "./component/fm.component";
import {ProfileComponent} from "./component/profile.component";
import {VideoComponent} from "./component/video.component";
import {ProgressBarComponent} from "./component/progress.component";
import {WebSocketService} from "./service/websocket.service";
import {SearchComponent} from "./component/search.component";
import {ObserverService} from "./service/observer.service";
import {RootService} from "./service/root.service";
import {ContextComponent} from "./component/context.component";
import {FileLineComponent} from "./component/file-line.component";
import {FilePlateComponent} from "./component/file-plate.component";
import {UploadFilesComponent} from "./component/upload-files.component";
import {InputModalComponent} from "./component/input-modal.component";
import {DropdownComponent} from "./component/dropdown.component";
import {FileInfoComponent} from "./component/file-info.component";
import {HomeComponent} from "./component/home.component";
import {SearchLineComponent} from "./component/search-line.component";
import {TextAreaModalComponent} from "./component/textarea-modal.component";
import {FieldComponent} from "./component/field.component";
import {ImageModalComponent} from "./component/image-modal.component";


@NgModule({
    imports:[
        BrowserModule,
        HttpModule,
        FormsModule,
        RoutingModule
    ],
    declarations: [
        RootComponent,
        NavComponet,
        OutputComponet,
        ProfileComponent,
        FMComponent,
        VideoComponent,
        ProgressBarComponent,
        SearchComponent,
        ContextComponent,
        FileLineComponent,
        FilePlateComponent,
        UploadFilesComponent,
        InputModalComponent,
        DropdownComponent,
        FileInfoComponent,
        HomeComponent,
        SearchLineComponent,
        TextAreaModalComponent,
        FieldComponent,
        ImageModalComponent
    ],

    providers:    [
        WebSocketService,
        OutputService,
        ObserverService

    ],
    bootstrap:    [ RootComponent ]
})
export class MainModule {}